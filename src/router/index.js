import Vue from 'vue'
import Router from 'vue-router'
import homePage from '@/pages/HomePage'
import aboutPage from '@/pages/aboutPage'
import contactPage from '@/pages/contactPage'
import servicePage from '@/pages/servicePage'


Vue.use(Router)

export default new Router({

  scrollBehavior (to, from, savedPosition) {
  return { x: 0, y: 0 }
},

  routes: [
    {
      path: '/',
      name: 'homePage',
      component: homePage
    },
    {
      path: '/about-us',
      name: 'aboutPage',
      component: aboutPage
    },
    {
      path: '/contact',
      name: 'contactPage',
      component: contactPage
    },
    {
      path: '/services',
      name: 'servicePage',
      component: servicePage
    },
  ]
})